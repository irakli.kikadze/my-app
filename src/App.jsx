import "./App.css";
import React from "react";
import Slider from "./slider/Slider";

const title = React.createElement(
  "h1",
  { style: { color: "#999", fontSize: "19px" } },
  "Solar system planets"
);

const planets = (
  <ul className="planets-list">
    <li>Mercury</li>
    <li>Venus</li>
    <li>Earth</li>
    <li>Mars</li>
    <li>Jupiter</li>
    <li>Saturn</li>
    <li>Uranus</li>
    <li>Neptune</li>
  </ul>
);

function App() {
  return (
    <div className="app">
      {title}
      {planets}
      <Slider></Slider>
    </div>
  );
}

export default App;
