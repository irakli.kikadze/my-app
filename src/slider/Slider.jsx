import React from "react";
import "./Slider.css";

const changeTheme = function () {
  const body = document.body;
  body.classList.toggle("dark");
};

const Slider = () => {
  return (
    <label className="switch" htmlFor="checkbox">
      <input type="checkbox" id="checkbox" onChange={changeTheme} />
      <div className="slider round"></div>
    </label>
  );
};

export default Slider;
